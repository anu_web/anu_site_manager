<?php
/**
 * @file
 * Assign META tags to nodes, vocabularies, terms and pages.
 */

/**
 * @addtogroup anu_site_manager
 * @{
 */

/*****************************************************************************
 * Menu callbacks / form builders, submit/validate functions.
 ****************************************************************************/
/**
 * Provides an overview of currently listed sites and their settings
 */
function anu_site_manager_profiles_pub_list() {
  global $theme_key;

  $profiles = anu_site_manager_load_profiles(FALSE) ;
  
  $description = '<p><strong>Publishing profiles</strong> allow you to define the way your content is published using the Acton theme for Drupal. A publishing profile contains a range of variables required for publishing an ANU site.</p>';

  $rows = array();

  $output = '<div id="anu-site-manager">';
  $output .= $description;

  if ($profiles) {
    $header = array(t('ANU ID'), t('Name'), t('Description'), t('Caching'), t('SSL status'), t('Enabled'), t('Mode'), array('data' => t('Operations'), 'colspan' => '3'),);

    foreach ($profiles as $profile) {      
      $data = array();
      $row_class = '';
      
      $data[] = array('data' => $profile->settings->anu_id, 'class' => 'identifier');
      $data[] = array('data' => $profile->profile_name, 'class' => 'name');
      $data[] = array('data' => check_plain($profile->description));
      
      // Caching
      if ($profile->options->caching) {
        $data[] = array('data' => 'On', 'class' => 'on');
      }
      else {
        $data[] = array('data' => 'Off', 'class' => 'off');
      }


      // SSL
      $ssl_status = 'None';
      
      if ($profile->options->return_ssl) {
        if ($profile->options->secure) {
          $ssl_status = 'All';
        } 
        else {
          $ssl_status = 'Styles only';
        }
      }
      else {
        if ($profile->options->secure) {
          $ssl_status = 'Data only';
        }
      }
      $data[] = array('data' => $ssl_status, 'class' => strtolower($ssl_status));
      
      // Enabled
      if ($profile->options->enabled) {
        $data[] = array('data' => 'Enabled', 'class' => 'on');
        $row_class = 'enabled';
      }
      else {
        $data[] = array('data' => 'Disabled', 'class' => 'off');
        $row_class = 'disabled';
      }
      
      // Mode
      if (isset($profile->dev_mode) && $profile->dev_mode == TRUE) {
        $data[] = array('data' => 'Development');
      }
      else {
        $data[] = array('data' => 'Normal');
      }
      
      // Set the edit column.
      $data[] = array('data' => l(t('edit'), 'admin/settings/anu-site-manager/publishing-profiles/edit/'. $profile->id));
      
      // Set the delete column.
      $data[] = array('data' => l(t('delete'), 'admin/settings/anu-site-manager/publishing-profiles/delete/'. $profile->id));
      
      $row = array(
        'data' => $data,
        'class' => $row_class,
        );
        
      $rows[] = $row;
    }

    if (empty($rows)) {
      $rows[] = array(array('data' => t('No sites defined.'), 'colspan' => '6', 'class' => 'message'));
    }

    $output .= theme('table', $header, $rows);
  }
  else {
    $output .= '<p><strong>No profiles defined</strong>! You must define a publishing profile to continue...</p>';
  }
  $base = base_path();
  $output .= '<div class="tools">'.l(t('Add a new Publishing Profile'), 'admin/settings/anu-site-manager/publishing-profiles/add').'</div>';
  $output .= '</div>';

  return $output;
}

/**
 * Settings form and submit handler
 */
function anu_site_manager_settings_form($form_state) {
  $form['help'] = array(
    '#value' => 'Set global configuration options.',
  );
  $form['notify_non_acton'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify if the active theme is not Acton-compatible'),
    '#default_value' => variable_get('anu_site_manager_notify_non_acton', TRUE),
    '#description' => t('This notifies the user if the current theme is not Acton or any theme compatible with the Acton theme.'),
    );
  $form['log_level'] = array(
    '#type' => 'select',
    '#title' => t('Logging level'),
    '#multiple' => FALSE,
    '#description' => t('Which messages to log.'),
    '#options' => array(
      ANU_SM_LOG_NONE => t('No logging'), 
      ANU_SM_LOG_WARN => t('Errors and warnings'),
      ANU_SM_LOG_REQUEST = t('Errors, warnings and remote requests'),
      ANU_SM_LOG_MESSAGE => t('Log everything (really not recommended)'),
      ),
    '#default_value' => variable_get('anu_site_manager_log_level', ANU_SM_LOG_WARN),
  );
  $form['debugging'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debug messages'),
    '#default_value' => variable_get('anu_site_manager_debug', FALSE),
    '#description' => "This outputs messages showing loading status, as well as logging remote server requests to the site log. <em>This should be off in a production environment</em>."
    );
  $form['ssl'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force SSL on all connections (deprecated)'),
    '#default_value' => variable_get('anu_site_manager_force_ssl', FALSE),
    '#description' => "This will make all connections to the remote style server use SSL for security. This setting is largely redundant, as there is more granular control in individual profiles.",
    );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

function anu_site_manager_settings_form_submit($form, &$form_state) {
  $form_state['storage']['values'] = $form_state['values'];
  $form_state['rebuild'] = TRUE;
  
  // SSL
  variable_set('anu_site_manager_force_ssl', $form_state['values']['ssl']);

  // Debugging
  variable_set('anu_site_manager_debug', $form_state['values']['debugging']);

  // Non-acton compatible theme notifications
  variable_set('anu_site_manager_notify_non_acton', $form_state['values']['notify_non_acton']);
  
  // Logging
  variable_set('anu_site_manager_log_level', $form_state['values']['log_level']);

  drupal_set_message('Your values have been saved');
}

/**
 * Theme: Edit settings
 */
function theme_anu_site_manager_settings_form($form) {
  drupal_set_breadcrumb(array(l(t('Home'), NULL), l(t('Administer'), 'admin'), l(t('Settings'), 'admin/settings'), l(t('ANU site manager'), 'admin/settings/anu-site-manager')));

  return drupal_render($form);
}

/**
 * Add/edit Publishing Profile form
 * @param $form_state the drupal form state variable
 * @param $sid a site ID to load. If none is provided, the form is an "add" form
 * instead.
 */
function anu_site_manager_profile_pub_edit_form($form_state, $sid = NULL) {

  $profile = NULL;
  if ($sid) {
    $profile = new actonProfile($sid);
  }
  
  // Constructs the search domain path from known values (profile domain,
  // profile url, then http host)
  if (is_object($profile)) {
    if (isset($profile->settings->search_domain)) {
      $search_domain = $profile->settings->search_domain;
      if (isset($profile->options->site_url)) {
        $profile_url = $profile->options->site_url;
      }
    }
    elseif (isset($profile->options->site_url)) {
      $search_domain = $profile->options->site_url;
      $profile_url = $profile->options->site_url;
    }
  }
  else {
    $search_domain = $profile_url = $_SERVER['HTTP_HOST'];
  }

  $form['help'] = array(
    '#value' => 'Enter the details for the profile...',
  );
  $form['profile_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Profile name'),
    '#required' => TRUE,
    '#size' => 64, 
    '#maxlength' => 64,
    '#default_value' => $sid ? $profile->profile_name : NULL,
    '#description' => "An administrative name for this profile. Used in the profile listings (64 chars max.)",
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#required' => TRUE,
    '#default_value' => $sid ? $profile->description : NULL,
    '#description' => "Optional description.",
  );
  $form['source'] = array(
    '#type' => 'fieldset',
    '#title' => t('Style server'),
  );  
  $form['source']['style_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Remote Style Server'),
    '#required' => TRUE,
    '#default_value' =>  $profile->options->style_server ? $profile->options->style_server : ANU_SM_DEFAULT_STYLE_SERVER,
    '#description' => "This should be the remote styles server provided by DOI, excluding http://.",
  );
  $form['source']['query_param'] = array(
    '#type' => 'textfield',
    '#title' => t('Additional query parameters'),
    '#required' => FALSE,
    '#default_value' =>  $profile->options->query_param ? $profile->options->query_param : '',
    '#description' => "Additional query parameters to send to the style server. These will be appended to the end of the server request, and should be in form <em>&amp;=parameter</em>.",
  );
  $form['source']['style_version'] = array(
    '#type' => 'select', 
    '#title' => t('Which version of the ANU styles to load'), 
    '#default_value' => $profile->options->style_version ? $profile->options->style_version : '3',
      '#options' => array(
        '2' => t('Two'), 
        '3' => t('Three'), 
       ),
      '#description' => t('This will default to <em>Three</em> for new sites, but will set to <em>Two</em> for sites converting from earlier module versions.'),
    );
  $form['profile'] = array(
    '#type' => 'fieldset',
    '#title' => t('Profile details'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['profile']['browser_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Browser title'),
    '#required' => TRUE,
    '#default_value' => $sid ? $profile->settings->browser_title : NULL,
    '#description' => "The title to appear in the browser's title bar. This will probably be the site name you provide when applying for an ID, however it can also be a shorter version. To change the actual site title, contact DOI.",
  );
  $form['profile']['anu_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Identifier'),
    '#required' => TRUE,
    '#default_value' => $sid ? $profile->settings->anu_id : NULL,
    '#description' => "This is the identifier provided to you when you apply for this site.",
  );
  // Should be used to generate canonical URLs for nodes.
  $form['profile']['site_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Default site URL'),
    '#required' => TRUE,
    '#default_value' => $profile_url,
    '#description' => "Root domain - this does not tell Drupal to find your site at this address (if you have only one site record, Drupal finds it automatically. If you have  more than one site record you <em>MUST</em> use the <a href=\"http://drupal.org/project/condition\">Conditions module</a>.)",
  );
  $form['profile']['resp_officer'] = array(
    '#type' => 'textfield',
    '#title' => t('Responsible Officer'),
    '#required' => TRUE,
    '#default_value' => $sid ? $profile->settings->resp_officer  : NULL,
    '#description' => "The position with final responsibility for the site. Should not be role name, not an individual.",
  );
  $form['profile']['resp_officer_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Responsible Officer URL/Email'),
    '#required' => TRUE,
    '#default_value' => $sid ? $profile->settings->resp_officer_address : '/contact',
    '#description' => "An email address or contact page for the Responsible Officer. Drupal provides a default contact form at \"/contact\" which you may wish to use. Email addresses are checked for validity, but not obfuscated.",
  );
  $form['profile']['site_contact'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Contact'),
    '#required' => TRUE,
    '#default_value' => $sid ? $profile->settings->site_contact : NULL,
    '#description' => "The principle contact for queries relating to the site (e.g. <em>Webmaster</em>).",
  );
  $form['profile']['site_contact_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Contact URL/Email'),
    '#required' => TRUE,
    '#default_value' => $sid ? $profile->settings->site_contact_address : '/contact',
    '#description' => "An email address or contact page for the Site Contact. Drupal provides a default contact form at \"/contact\" which you may wish to use. Email addresses are checked for validity, but not obfuscated.",
  );
  $form['profile']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => $sid ? $profile->options->enabled : TRUE,
    '#description' => "Deselect to disable.",
  );
  $form['optional'] = array(
    '#type' => 'fieldset',
    '#title' => t('Additional settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['optional']['about_page'] = array(
    '#type' => 'textfield',
    '#title' => t('About Page'),
    '#required' => FALSE,
    '#default_value' => $sid ? $profile->settings->about_page : '/about',
    '#description' => "(optional) The <em>About</em> page is typically used to indicate the website compliance with standards and any help or instructions specific to using the site. It is NOT the entity's <em>About us</em> page.",
  );
  $form['optional']['show_search'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Search Box'),
    '#default_value' => $sid ? $profile->settings->show_search : FALSE,
    '#description' => "Display the ANU search box. This is different from the Drupal search box, which can be enabled under Block settings.",
  );
  $form['optional']['search_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Search domain'),
    '#required' => FALSE,
    '#default_value' => $sid ? $search_domain : '',
    '#description' => "(optional) The domains from which to return Funnelback search results, separated with commas. Only works if ANU search is enabled, above. If nothing is entered, this defaults to the Site URL (above), then the current Hostname. If you have a different URL, or more than one, for search results you MUST set this value to the desired domain(s).",
  );
  $form['ssl_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('SSL'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['ssl_fieldset']['secure'] = array(
    '#type' => 'checkbox',
    '#title' => t('Connect to the remote styles server using SSL'),
    '#default_value' => $sid ? $profile->options->secure: '',
    '#description' => "Whether to make SSL connections when retrieving HTML components from the remote server. This only affects your server connection to the remote server. It does not affect the SSL status of your site (which requires a separate configuration through your web server) and the links to resources returned will not be SSL enabled (see <em>Return SSL ready styles</em>, below). <strong>This setting should almost always be off.</strong>",
  );
  $form['ssl_fieldset']['return_ssl'] = array(
    '#type' => 'checkbox',
    '#title' => t('Return SSL-ready styles'),
    '#default_value' => $sid ? $profile->options->return_ssl: '',
    '#description' => "Forces your site to always return SSL-enabled styles from the remote styles server(i.e. links with \"https://.\" in front of them). It does not affect the SSL status of your site, nor the connection you make to retrieve the styles (see <em>Connect to the remote styles server using SSL</em> above). This will have a negative performance impact, so only enable if you are using SSL connections (\"https://\") on your site.",
  );
  $form['performance'] = array(
    '#type' => 'fieldset',
    '#title' => t('Performance'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['performance']['caching'] = array(
    '#type' => 'checkbox',
    '#title' => t('Caching'),
    '#description' => t('Cache generated HTML and other data usind Drupal\'s native caching mechanism'),
    '#default_value' => $sid ? $profile->options->caching : TRUE,
  );

  $form['meta'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default metadata'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['meta']['meta_subject'] = array(
    '#type' => 'textarea',
    '#title' => t('Subject keywords'),
    '#required' => FALSE,
    '#default_value' => $sid ? $profile->meta->subject : '',
    '#description' => "Subject keywords, separated by comma's. This will be used on pages where no metadata exists (like a dynamic home page). Individual node terms will override this, as can some modules. Note that most search engines ignore this value.",
    '#cols' => 60,
    '#rows' => 5,
  );
  $form['meta']['meta_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#required' => FALSE,
    '#default_value' => $sid ? $profile->meta->description : '',
    '#description' => "Default site description. This will be used on pages where no metadata exists (like a dynamic home page). Individual node teasers will override this, as can some modules. A common use of this value is Google's page descripton.",
    '#cols' => 60,
    '#rows' => 5,
  );
  $form['dev'] = array(
    '#type' => 'fieldset',
    '#title' => t('Development tools'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['dev']['alert_bar'] = array(
    '#type' => 'textfield',
    '#title' => t('Alert bar'),
    '#required' => FALSE,
    '#default_value' => $sid ? $profile->settings->alert_bar : '',
    '#description' => "The alert bar should be used sparingly. It is designed to indicate environments that should not be publicly available (such as development, acceptance, test etc.) and for limited site-wide alerts. The alert bar should <strong>not</strong> be used for news and items of interest.",
    '#maxlength' => 256,
  );
  $form['dev']['dev_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Development mode'),
    '#description' => t('Run this site profile in development mode (not recommended for production sites). Development mode overrides some of the options above.'),
    '#default_value' => $sid ? $profile->mode : '',
  );

  $form['sid'] = array(
    '#type' => 'hidden',
    '#value' => $sid ? $profile->id : NULL,
  );

  if (module_exists('condition_requirements')) {
    $form = array_merge($form, module_invoke('condition', 'selection_form', $profile->options->conditions));
  } 
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  // utility button to reset the information
  $form['clear'] = array(
    '#type' => 'submit',
    '#value' => 'Reset form',
    '#validate' => array('anu_site_manager_edit_site_clear'),
  );

  return $form;
}
/**
 * Theme: Add or edit a virtual site.
 */
function theme_anu_site_manager_profile_pub_edit_form($form) {
  drupal_set_breadcrumb(array(l(t('Home'), NULL), l(t('Administer'), 'admin'), l(t('Settings'), 'admin/settings'), l(t('ANU site manager'), 'admin/settings/anu-site-manager')));

  if (isset($form['#item'])) {
    drupal_set_title(t('Edit publishing profile: %name', array('%name' => $form['#item']->name)));
  }
  else {
    drupal_set_title(t('Add a publishing profile'));
  }

  return drupal_render($form);
}
/**
 * Validation handler for our Reset button. Setting
 * the $form_state['rebuild'] value to TRUE, clears the form and also
 * skips the submit handler.
 */
function anu_site_manager_edit_site_clear($form, &$form_state) {
    $form_state['rebuild'] = TRUE;
}

/**
 * Validation handler for the Publishing profile edit form
 */
function anu_site_manager_profile_pub_edit_form_validate($form, &$form_state) {

  // if this is new config, check we haven't added it already
  if (!$form_state['values']['sid']) {
    $profiles = anu_site_manager_load_profiles();
    foreach ($profiles as $profile) {
      if ($form_state['values']['anu_id'] == $profile->anu_id) {
        drupal_set_message('',t('There is already a profile with this ANU ID ('. $profile->settings->browser_title.'). While duplicates are possible on the same URL, you might get unexpected behaviour.'));
      }
    }
  }
  
  if (strstr($form_state['values']['style_server'], 'http://')) {
    $form_state['values']['style_server'] = ereg_replace("(http?)://", "", $form_state['values']['style_server']);
  }

  // validate email fields
  $fields = array('resp_officer_address', 'site_contact_address');
  foreach ($fields as $field) {
    $email = $form_state['values'][$field];
    $is_email = _anu_site_manager_is_email($email);
    if (!$is_email) {
      drupal_set_message('The value you entered for '. $field.' is not a valid email address. Unless you are using a URL, you may wish to fix this.');
    }
  }

  // validate Site URL and remove http://
  if ($form_state['values']['site_url'] == '') {
    form_set_error('', t('You must provide a default URL.'));
  }
  else {
    if (strstr($form_state['values']['site_url'], 'http://')) {
      $form_state['values']['site_url'] = ereg_replace("(http?)://", "", $form_state['values']['site_url']);
    } elseif (strstr($form_state['values']['site_url'], 'https://')) {
      $form_state['values']['site_url'] = ereg_replace("(https?)://", "", $form_state['values']['site_url']);
    }
  }

  // validate search domains and remove http://
  if ($form_state['values']['search_domain']) {
    $domains = explode(', ', $form_state['values']['search_domain']);
    foreach ($domains as $key => $domain) {
      $domain = trim($domain);
      $domains[$key] = ereg_replace("(http?)://", "", $domain);
    }
    $form_state['values']['search_domain'] = implode(", ",$domains);
  }

  // other basic validation
  if ($form_state['values']['anu_id'] == '') {
    form_set_error('',t('You must provide an ANU site ID (You may need to <a href="'.ANU_SM_STYLE_GUIDE.'">submit an application</a>).'));
  }
  if ($form_state['values']['browser_title'] == '') {
    form_set_error('', t('You must provide a short name.'));
  }

}

function anu_site_manager_profile_pub_edit_form_submit($form, &$form_state) {
  $form_state['storage']['values'] = $form_state['values'];
  $form_state['rebuild'] = TRUE;

  module_load_include('inc', 'anu_site_manager', 'includes/actonProfile');
  $record = new actonProfile();
  $record->save_form($form_state['values']);
  
  // Flush the cache completely
  anu_site_manager_empty_cache();

  drupal_goto('admin/settings/anu-site-manager');
}

/**
 * Deletes a site from the database
 */
function anu_site_manager_profile_pub_delete_form($form_state, $sid) {
  $profile = new actonProfile();
  $profile->load($sid);

  $form['help'] = array(
    '#value' => 'You are about to delete <strong>'. $profile->name.'</strong>. Do you wish to continue? There is no undo.',
  );
  $form['sid'] = array(
    '#type' => 'hidden',
    '#value' => $sid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Delete this site',
  );
  // utility button to reset the information
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => 'Cancel',
    '#validate' => array('anu_site_manager_delete_site_cancel'),
  );

  return $form;
}

/**
 * Cancels the delete operation
 */
function anu_site_manager_delete_site_cancel() {
  drupal_goto('admin/settings/anu-site-manager');
}

/**
 * Deletes a site
 */
function anu_site_manager_profile_pub_delete_form_submit($form, &$form_state) {
  $profile = new actonProfile();
  $profile->delete($form_state['values']['sid']);
  drupal_goto('admin/settings/anu-site-manager');
}

/**
 * Creates a page to flush the cache
 */
function anu_site_manager_cache_page() {
  $output = NULL;
  
  $output = '<a href="/admin/settings/anu-site-manager/caching/flush">Flush ANU Site Manager page cache</a>';
  
  return $output;
}
/**
 * Creates a page of debugging information
 */
function anu_site_manager_debug_page() {
  $output = NULL;
  
  $output = '<p>If you are experiencing a problem with your Acton installation, you may be requested to copy and email the output of this page.</p>';
  
  $profile = anu_site_manager_load_active();
  $all = anu_site_manager_load_profiles();
  
  /**
   * Version information
   */
  $version = '<h3>Installed versions</h3>';
  $version .= '<p>ANU Site Manager: '.ANU_SM_VERSION.'<br />';
  $version .= 'ANU Site Manager API: '.ANU_SM_API_VERSION.'<br />';
  $version .= 'ANU Site Manager Template: '.ANU_SM_TEMPLATE_VERSION.'<br />';

  $no = anu_site_manager_acton_version();
  if (isset($no)) {
    $version .= 'Acton: '. $no;
  } else {
    $version .= 'Could not detect Acton version, it may not be installed.';
  }

  $version .= '</p>';
  
  $output .= $version;
  
  /**
   * Remote server test
   */
  $connection = anu_site_manager_get_remote_file('banner', $profile, array('dummy' => TRUE));
  $output .= '<h3>Remote server test</h3>';
  if (module_exists('devel')) {
    $output .= kdevel_print_object($connection);
  }
  else {
    $conn_table = array();
    $header = array('Type', 'Status');
    $conn_table[] = array('Request', $connection->request);
    $conn_table[] = array('Code', $connection->code);
    $conn_table[] = array('Status', $connection->status_message);
    $conn_table[] = array('Length', $connection->headers['Content-Length']);
    if ($connection->error) { 
      $conn_table[] = array('Error', $connection->error); 
    };
    $output .= theme('table', $header, $conn_table);
  }
  
  /**
   * Current profile object
   */
  $output .= '<h3>Current profile</h3>';
  if (module_exists('devel')) {
    $output .= kdevel_print_object($profile);
  }
  else {
    $output .= '<pre>'.print_r($profile, TRUE).'</pre>';
  }

  return $output;
}