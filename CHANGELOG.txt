CHANGELOG

Version 2.1
--------------------------------
- Added custom HTML header options for the Acton theme to use.

Version 2.0
--------------------------------
- FEATURE: Version 3 of the ANU styles are supported, via a switchable toggle in each
  profile
- FEATURE: Style server locations are now set in individual profiles, this means (for
  example) that you can use two style servers (a live and test version) without
  disrupting your live site (Conditions and Virtual Sites modules required for
  more than one profile per site).
- FEATURE: Individual query strings can be overridden on a per-profile basis.
- The backend has been almost completely rewritten for maintainability.
- FEATURE: Profiles now operate in "modes". The default mode is "normal", however if
  something goes wrong, the site manager goes to a Fallback profile. Individual
  profiles can also be put in Development mode.
- FEATURE: Better performance on high-traffic sites. Completely built template 
  objects are now cached using Drupal's default caching mechanism on a page-by-
  page basis.
- FEATURE: SSL support rewritten. It is now possible to request SSL-ready styles for
  use on your site without making a slow SSL request to the styles server.
- FEATURE: Connection debugging. Visit the debugging tab to see the current status
  of the connection to the remote styles server.
- Better logic in the theme compatibility check.
- Faster
- Extra debugging information
- Compatible with ANU Remote Styles Cache 2.0
- Automatic upgrade from version 1.6 when running update.php
- Remote style server connections are now fault-tolerant and will not throw a
  PHP error when failing to connect. Errors are logged to the watchdog.
- Simpler $profile->save logic when using forms
- Reinstated API timeout settings for remote styles requests
- Not having a style server set will no longer cause the theme loading process
  to fail.
- Fixed an issue where some site configurations would load the module early and
  throw a fatal error (thanks Steve L.)
- Fixed an issue where caching settings were ignored
- Fixed an issue where subject and description metadata was never being written 
  to the header.
- Fixed support for Conditions
- Fixed ANU Search display options
- Many minor internal consistency and documentation updates

Removed in 2.0
- Token, Filters and Nodewords support is being moved to add-on modules.
- Google Analytics support has been dropped.
- The plug-in example module has been removed
- The site manager no longer checks for missing ANU-enabling modules.

Version 1.5
-------------------------------
- The API has changed. Please check the API documentation
- Subject and Description metadata can now be set to default
- Added nodewords integration (Issue #2) - note this is incomplete, and may
  not behave as expected. Also, it scrubs most nodewords content
- Added version information to the ANU Site Manager page
- Added a mechanism for warning of update changes
- Compatibility changes for ANU jQuery scripts to avoid duplicate jQuery libs
- Security patch for values entered into Search Domains
- Improved email verification
- Fixed an issue where new sites could not be added when clean URLs were off.
- Fixed an issue where not having Conditions installed would break the  
  Publishing profile page.
- Fixed HTTPS connections for secure profiles.
- Added a new method to load a remote file, for integration with ANU Remote 	
	Styles Cache
- Minor code changes to support the ANU remote styles cache
- Fixed a call time pass by reference warning in the install file
- Minor updates for PHP 5.3 compatibility
- Performance improvements

Version 1.4
-------------------------------
- Fixed an issue where Site URLs were not being pre-filled when a Search 
	Domain did not exist
- Fixed an issue where sites at non-root locations could not create new 
	publishing profiles.
- Fixed a problem where it was impossible to create new publishing profiles, 
	when one did not already exist.

Version 1.3
-------------------------------
- NEW! The Search Domains parameter has been added. This allows you to return
	search results from different or multiple sites, when using Funnelback.
- NEW! SSL support, either Automatic, or Forced for individual configurations 
	(experimental)
- NEW! Page titles are compatible with the Page Titles module
- NEW! The metadata compiled by the ANU site manager is now available to the 
	template through $vars->site['metadata']. Previously, this was striped after
	the page prereq's were built.
- CHANGE: "Sites" are now called "Publishing Profiles", for compatibility with 
	the forthcoming "Unify" module.
- CHANGE: Adding a new Publishing Profile ("site") is now a link on the 
	Publishing Profiles page, not a tab
- CHANGE: It is now possible to use identical ANU IDs with different 
	publishing profiles. This allows you to have different Publishing Profiles 
	on the same URL, without needing another Site ID (for example, to set
	different Responsible officers, or change the SSL settings, etc.)
- CHANGE: A number of usability improvements have been made to the
	installation process:
	- The ANU Styles Server variable is now set to a default on install, which 
		makes setup easier.
	- Removed the dependancy on Conditions for single-profile installations
	- Fixed the "enabled" toggle (it actually works now)
	- Tidied up the code that loads and saves profile objects
	- Better errors
	- More descriptive documentation
- CHANGE: Some of the variable names used in profiles have changed. You may 	
	to update any API calls to hook_anu_site_manager_meta_alter().
- Improved the documentation for the API
- Many internal functions and form callbacks have been changed and renamed to
	accommodate a more flexible, profile driven approach.
- The page title in the browser properly respects Drupal's page title, instead 
	of arbitrarily deciding where you are.
- Fixed an issue where the Search Scope was not being passed to the search
	engine
- Removed redundant length specs from install schema
- Fixed URI metadata output for admin pages
- Improved the handling of the Style Server variable in the code.
- Security Update: Text entered in the Alert Bar was not cleaned before 
	output.
- Started Token implementation (not yet complete)
- Started "Development mode" implementation (not yet complete)

Version 1.2 - SKIPPED

Version 1.1
------------------------------
- NEW! A site URL can be declared as default for each configuration. This sets 
  Identifier and Canonical URLS.
- Updated the code for the ANU search box
- Remote style server is pre-filled by default with 'styles.anu.edu.au/_anu'
- Tidied up some code

Version 1.0
------------------------------
- NEW! "ANU Strings" input filter is available at admin/settings/filters. 
	This filter will replace [anu:SiteShortName] in node text with the actual site
	name, and also works with other ANU Site Manager variables. Add the filter to
	Filtered HTML, then see Input Format help for more information.
- NEW! Basic Google Analytics support added (though using the Drupal Google 
	Analytics module is probably better)
- NEW! Region classes are now added dynamically through calls in 
	hook_preprocess_page() This allows you to programmatically override the WIDTH 			
	and LAYOUT of these regions from your sub-theme's template.php file 	
	(specifically with application to ANU grid classes).
- NEW! Debugging can be turned on via the UI. This will output messages using 
	drupal_set_message, as well as writing all Styleserver requests to the log. 
	(Don't use in production).
	
	OTHER FIXES and updates
- API functions now use the full module name in their hooks
	for consistency with the Drupal API. e.g...
	HOOK_anu_sm_meta_alter => HOOK_anu_site_manager_meta_alter
	This will not affect you unless you have already implemented these hooks in a 
	module or subtheme.
- The favicon option has been renabled, to allow you to DISABLE it, as the ANU
	styles provide their own favicon.
- Lots of unused page variables removed from $vars in hook_preprocess_page
- A version number string is now included in <head>, showing installed ANU 
	Drupal theme/module versions.
- Cleaned up some documentation and code explanations
- Removed a redundant call to get the Site URL from the remote server
- Fixed an issue where node links would wrap badly
- Fixed tab margins in tabs.css
- Better colours for status messages
- Compatibility updates for the ANU Content Styles module
- Compatibility updates for using the Admin module and Rubik theme for 
	administration
- Some minor non-destructive changes to theme_functions

Version 0.8.1-beta
------------------------------
- Fixed hook_block_preprocess accidentally omitted in 0.8
- Updated Readme and API docs

Version 0.8-beta
------------------------------
- Subject metadata is now pre-filled with page terms by default
- The breadcrumb functionality has been rewritten, and now conforms to the
	brand. Most of the original options have been removed, however you can now set 
	a custom site name to appear instead of "home".
- The order of content elements has changed in page.tpl.php
	The Content Top region has moved below the title, and the tabs have been moved
	to above the main content section. Additionally, a feature region is now 
	available above the title area. This improves the relationships between page 
	elements, and stops the placement of content other than the feature above the 
	page title.
	[But see note below...]
- API updated to 1.2
	- New API function anu_site_manager_clean_html() for removing illegal content
		from metadata fields
- NEW Check to ensure ANU module extensions are installed, and warning if they
are not.
- "About this site" is now optional, and the help text has been updated to reflect
	this.
- Better styling on Status Report table headers, module information
- Screenshot added for Theme display page
- Fixed display of the Alert bar from 0.7
- Fixed an issue where an extra div was blocking validation and causing the footer
to display incorrectly.
- Fixed a problem where HTML could be inserted into a metadata field (TEST)
- Removed an unused 'navbar' region from the .info file in the ANU 2010 theme
- The maintenance.tpl.php template has been removed, as it wasn't doing anything.
	It would be nice to have a default ANU maintenance page we can implement, but
	there isn't one yet. In the interim, your site will probably just use Garland.

NOTE on the FEATURE block
The ANU styles feature block is NOT implemented in this (or possibly any future)
release. There are issues with its accessibility & cross-browser compatibility.
I have a partially complete Views implementation of it, however I wont be doing
any more development on it. If someone else would like to try Im happy to hand 
over the code.


Version 0.7.1-alpha
---------------------------
- New Block classes (fixes broken classes)
	.block-<module>-<delta> replaces .region-count-<block-zebra>
	.region-<region-name> replaces .region-count-<block-id

- Fixed an issue where the $node was not available to the templating function
- Fixed an issue where the search box wasn't displaying
- Fixed an issue where empty menu classes caused validation errors
- Fixed an issue where HTML entities weren't properly encoding during output

Version 0.7-alpha    9/1/09
---------------------------
- NEW Alert Bar function (to help you distinguish between production and dev
sites)
- REMOVED config.ini and its functionality. All settings are now set through 
the Site Manager 
- Responsible Officer and Site Contact correctly check for email addresses, 
AND also add "mailto:" where appropriate
- Added a constant to force a full reset on each page load. Should be FALSE 
in prod environments, but TRUE if debugging required.
- Search box will still display if no menu is configured in the right sidebar
- Spacing around some minor page elements improved
- Debug functions
- Minor bug fixes

Version 0.6-alpha    6/12/09
----------------------------
- New email validation for site settings
- New API
- Code vastly refactored, many functions moved into Site Manager
