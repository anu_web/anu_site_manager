<?php
/**
 * ANU site manager theme implementations
 * 
 * These provide various code sections to be used in the Acton theme.
 */
 
/**
 * Return the themed representation of the meta content for the page head
 * @param
 * @return the themed output
 */
function theme_anu_site_manager_page_requisites_meta($vars) {
  $output = NULL;
  $profile = $vars[0];

  $generated_meta = anu_site_manager_get_content('meta', $profile);
  $output  = '<!-- ANU Generated Metadata -->';
  $output .= $generated_meta;
  
  return $output;
}

/**
 * Return the themed representation of the page banner
 * @param
 * @return the themed output
 */
function theme_anu_site_manager_page_requisites_banner($vars) {
  $output = NULL;
  $profile = $vars[0];
  
  $output = anu_site_manager_get_content('banner', $profile);
  
  return $output;
}

/**
 * Return the themed representation of the page footer
 * @param
 * @return the themed output
 */
function theme_anu_site_manager_page_requisites_footer($vars) {
  $output = NULL;
  $profile = $vars[0];
  
  $output = anu_site_manager_get_content('footer', $profile);
  
  return $output;
}

/**
 * Return the themed representation of the doctype
 * @param
 * @return the themed output
 */
function theme_anu_site_manager_page_requisites_doc_type($vars) {
  $output = NULL;
  $profile = $vars[0];
  
  $output = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">';
  
  return $output;
}

/**
 * Return the themed representation of the page head metadata
 * @param
 * @return the themed output
 */
function theme_anu_site_manager_page_requisites_head($vars) {
  $output = NULL;
  $profile = $vars[0];
  
  $output = '
<title>'. $profile->meta->page_title.'</title>
<meta name="DC.title" content="'. $profile->meta->page_title.' - '. $profile->settings->browser_title.' - ANU" />
<meta name="DC.identifier" scheme="URI" content="http://'. $profile->options->site_url.'/'. $profile->meta->page_uri.'" />
<meta name="DC.description" content="'. $profile->meta->description.'" />
<meta name="DC.subject" content="'. $profile->meta->subject.'" />
<meta name="DC.date.modified" scheme="ISO8601" content="'. $profile->meta->date_modified.'" />
<meta name="DC.date.review" scheme="ISO8601" content="'. $profile->meta->date_review.'" />
<meta name="DC.creator" content="'. $profile->meta->creator.'" />
<meta name="DC.creator.email" content="/contact" />
<meta name="description" content="'. $profile->meta->description.'" />
<meta name="keywords" content="'. $profile->meta->subject.'" />
<!--Additional Metadata-->
<meta name="DC.date.created" scheme="ISO8601" content="'. $profile->meta->date_created.'" />
<meta name="DC.language" scheme="RFC3066" content="en" />';
  
  return $output;
}

/**
 * Return the themed representation of the page head js additions
 * @param
 * @return the themed output
 */
function theme_anu_site_manager_page_requisites_js($vars) {
  $output = NULL;
  $profile = $vars[0];
  
  $output  = '<!-- jq -->'."\n";
  $output .= '<script type="text/javascript"> var $anujq = jQuery; </script>'."\n";
  $output .= anu_site_manager_get_linked_file('script', 'jquery.hoverIntent.js', 'scripts', $profile)."\n";
  $output .= anu_site_manager_get_linked_file('script', 'anu-common.js', 'scripts', $profile)."\n";
  $output .= '<!-- ejq -->'."\n";
  
  return $output;
}

/**
 * Return the themed representation of the alert bar
 * @param
 * @return the themed output
 */
function theme_anu_site_manager_page_requisites_alert_bar($vars) {
  $output = NULL;
  $profile = $vars[0];
  
  if (isset($profile->settings->alert_bar) && $profile->settings->alert_bar != '') {
    $output = '<div id="devlmsg">'. $profile->settings->alert_bar.'</div>';
  }

  
  return $output;
}

/**
 * Return the themed representation of the search box
 * @param
 * @return the themed output
 */
function theme_anu_site_manager_page_requisites_search_box($vars) {
  $output = NULL;
  $profile = $vars[0];

  if (isset($profile->settings->show_search) && $profile->settings->show_search == TRUE){
    $output = '
      <div class="search-box">
         <p>Search '. $profile->settings->browser_title.'</p>
         <form action="http://search.anu.edu.au/search/search.cgi" method="post">
         <div>
            <input name="scope" type="hidden" value="'. $profile->settings->search_domain.'" />
            <input name="collection" type="hidden" value="anu_search" />
            <label for="local-query"><span class="nodisplay">Search query</span></label><input class="search-query" name="query" id="local-query" size="15" type="text" value="" />
            <label for="search"><span class="nodisplay">Search</span></label><input class="search-button" id="search" title="Search" type="submit" value="GO" /><br/>
            <a href="http://search.anu.edu.au/search/search.cgi?collection=anu_search&amp;form=advanced&amp;scope='. $profile->settings->search_domain.'">Advanced search</a>
         </div>
         </form>
      </div>
      ';
  }
  else {
    $output = FALSE;
  }
  
  return $output;
}

/**
 * Return the themed representation of the update details
 * @param
 * @return the themed output
 */
function theme_anu_site_manager_page_requisites_update($vars) {
  $output = NULL;
  $profile = $vars[0];
  
  // dont display the "about" link if one is not provided
  $about = ($profile->settings->about_page) ? '<span class="right noprint"><a href="'. $profile->settings->about_page.'"><strong>about this site</strong></a></span>' : '' ;

  $output = '
    <!-- noindex -->
    <div id="update-wrap">
      <div id="update-details">
        <p class="sml-hdr">
        '. $about.'
        Updated: <strong>'. $profile->meta->date_mod_readable.'</strong><span class="hpad">/</span>
        Responsible Officer:&nbsp;&nbsp;<a href="'. $profile->settings->resp_officer_address.'"><strong>'. $profile->settings->resp_officer.'</strong></a> <span class="hpad">/</span>
        Page Contact:&nbsp;&nbsp;<a href="'. $profile->settings->site_contact_address.'"><strong>'. $profile->settings->site_contact.'</strong></a>
        </p>
      </div>
    </div>
    <!-- endnoindex -->';
  
  return $output;
}

/**
 * Return the themed representation of the footer js links
 * @param
 * @return the themed output
 */
function theme_anu_site_manager_page_requisites_footer_js($vars) {
  $output = NULL;
  $profile = $vars[0];
  
  $output = anu_site_manager_get_linked_file('script', 'anu-menu.js', 'scripts', $profile);
  
  return $output;
}

/**
 * Return the themed representation of Debug information
 * @param
 * @return the themed output
 */
function theme_anu_site_manager_page_requisites_debug($vars) {
  $output = NULL;
  $profile = $vars[0];
  
  $output  = '<meta name="ANU-SM.version" content="'.ANU_SM_VERSION.'" />';
  $output .= '<meta name="ANU-SM.api" content="'.ANU_SM_API_VERSION.'" />';
  $output .= '<meta name="ACTON.version" content="#fixme" />';
  
  return $output;
}