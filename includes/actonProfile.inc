<?php
/**
 * Acton Site Manager object definition
 *
 * Need to extend this object?
 * To add additional properties, you need to update the following locations:
 *    - the __construct, set, and form_to_object methods below
 *    - the relevant form in anu_site_manager.admin.inc
 *
 * @param $id the internal site ID (serialised)
 * @param $profile_name the profile's local name
 * @param $description a local description of the profile
 * @param $type the type of profile. Currently this is built for, and only
 *              supports, 'publish'ing profiles
 * @param $dev_mode whether to operate the site in development mode for this profile
 * @param $settings these are passed to the templates, and are mostly the basic
 *              basic ANU template settings
 * @param $options configuration settings for the profile
 *              - style_server: the server to retrieve styles from
 *              - query_param: query parameters to add to the request
 *              
 *  - further information is provided in the constructor function
 */
 
class actonProfile {
  
  public $id; // The profile id
  public $profile_name; // The profile name
  public $description;
  public $type = 'publish';
  
  // profile mode (can be one of fallback, dev, live)
  public $mode = 'fallback';

  public $settings; // the settings to be used in the template
  public $options; // other options, local to this site
  public $meta; //page metadata
  
  public $db_flag; // used to track write success
  
  /**
   * Constructor function
   * 
   */
  public function __construct($id = NULL) {

    if (isset($id)) {
      // we are on an existing object
      $object = $this->load($id); 
      
      $options = unserialize($object->options);
      $settings = unserialize($object->settings);
      if(isset($this->options->dev_mode) && $this->options->dev_mode == TRUE){
        $this->mode = 'dev'; 
      }
      else {
        $this->mode = 'live';
      }
      
      /**
       * Basic object properties
       *
       * @param id = the local machine id
       * @param profile_name = a human readable name
       * @param description = an optional description for the admin interface
       */
      $this->id = $object->sid;
      $this->profile_name = $this->clean($object->profile_name);
      $this->description = $this->clean($object->description);

      /**
       * Main ANU defined settings
       *
       * @param anu_id = the ANU site ID
       * @param browser_title = the title used in the browser
       * @param resp_officer = the responsible officer, usually a School head or similar
       * @param resp_officer_address = email or URL to contact the responsible officer
       * @param site_contact = the website contact
       * @param site_contact_address = email or URL to contact the site contact
       * @param about_page = a url of a page which describes the site you are on
       * @param alert_bar = text to go in the optional alert bar
       * @param search_domain = string containing domains to pass to funnelback
       * @param show_search = whether to show the ANU search box or not
       */
      $this->settings->anu_id = $settings->anu_id; 
      $this->settings->browser_title = $this->clean($settings->browser_title);
      $this->settings->resp_officer = $this->clean($settings->resp_officer);
      $this->settings->resp_officer_address = _anu_site_manager_check_address_format($settings->resp_officer_address, 'default', 'mailto:');
      $this->settings->site_contact = $this->clean($settings->site_contact);
      $this->settings->site_contact_address = _anu_site_manager_check_address_format($settings->site_contact_address, 'default', 'mailto:');
      $this->settings->about_page = $settings->about_page;
      $this->settings->alert_bar = $this->clean($settings->alert_bar);
      $this->settings->search_domain = $this->clean($settings->search_domain);
      $this->settings->show_search = $settings->show_search;

      /**
       * Configuration
       */
      $this->options->style_server = $options->style_server;
      $this->options->query_param = $options->query_param;
      $this->options->style_version = $options->style_version;
      $this->options->site_url = $this->clean($options->site_url);
      $this->options->secure = $options->secure;
      $this->options->return_ssl = $options->return_ssl;
      $this->options->enabled = $options->enabled;
      $this->options->conditions = $options->conditions;
      $this->options->caching = $options->caching;
      $this->options->dev_mode = $options->dev_mode;
      
      /**
       * Metadata
       */
      $this->meta->subject = $this->clean($settings->meta_subject);
      $this->meta->description = $this->clean($settings->meta_description);
      $this->meta->date_created = date(DATE_ISO8601); // now
      $this->meta->date_modified = date(DATE_ISO8601); // now
      $this->meta->date_mod_readable = date("j F Y"); // today
      $this->meta->page_uri = ltrim($_SERVER['QUERY_STRING'], 'q=');
      $this->meta->creator = $this->settings->resp_officer;
    
      unset($this->meta->extra['robots']);
      
      // override some stuff with default config
      // Force SSL connections if we told it to
      if (variable_get('anu_site_manager_force_ssl', FALSE) == TRUE) {
        $this->options->secure = TRUE;
      }
      
      if (!isset($this->options->site_url)) {
        $this->options->site_url = $_SERVER['HTTP_HOST'];
      }
      
      if ($this->mode == 'dev') {
        // only override specific properties
        if (isset($settings->alert_bar)) {
          $this->settings->alert_bar = $this->clean($settings->alert_bar);
        }
      } 
      
    }
else {
      $this->mode_set_properties('fallback');
    }
  }
  
  /**
   * Load a profile from the database, by sid, into the object
   */
  public function load($id) {    
    $db_object = db_fetch_object(db_query("SELECT * FROM {anu_site_manager_profiles} WHERE sid = %d", $id));
    return $db_object;
  }
  
  /**
   * Wrapper to save a form to an object
   */
  public function save_form($form) {
    $this->form_to_object($form);
    $this->save();
  }
  /**
   * Convert a fully built object into schema format and save it
   * @param the form from the editing page
   */
  public function save() {
    $this->db_flag = FALSE;
    
    /**
     * Build a new object to write to the database
     */
    $record = (object) array();
    
    if (isset($this->id)) {
      $record->sid = $this->id;
      $op = 'update';
    }
    else {
      $op = 'create';
    }
    $record->type = $this->type;
    $record->profile_name = $this->profile_name;
    $record->description = $this->description;
    $record->settings = $this->settings;
    $record->options = $this->options;
      
    /**
     * Save to the database
     */

    switch ($op) {
      case 'create':
        $result = drupal_write_record('anu_site_manager_profiles', $record);
        break;
      case 'update':
        $result = drupal_write_record('anu_site_manager_profiles', $record, 'sid');
        break;
    }

    //check that the record was written correctly
    if ($result) {
      switch ($result) {
        case SAVED_NEW:
          drupal_set_message('The profile "'. $this->profile_name.'" was successfully saved.');
          $this->db_flag = TRUE;
          break;
        case SAVED_UPDATED:
          drupal_set_message('The profile "'. $this->profile_name.'" was successfully updated.');
          $this->db_flag = TRUE;
          break;
      }
    }
    else {
      drupal_set_message(ANU_SM_ERROR_5, 'warning',FALSE);
    }
  }
  
  public function delete($id = NULL) {
    db_query("DELETE FROM {anu_site_manager_profiles} WHERE sid = '%s'", $id);
    drupal_set_message('The profile has been deleted');
  }
  
  /**
   * Set initial development properties on a newly created object
   */
  public function mode_set_properties($mode) {
     if ($mode == 'fallback') {
        $this->profile_name = 'Fallback mode';
        $this->description = 'The built in development/fallback profile';
        $this->options->dev_mode = 1;

        // set template properties
        $this->settings->anu_id = '1999';
        $this->settings->browser_title = 'Development';
        $this->settings->resp_officer = 'Responsible officer';
        $this->settings->resp_officer_address = '#';
        $this->settings->site_contact = 'Site contact';
        $this->settings->site_contact_address = '#';
        $this->settings->about_page = '#';
        $this->settings->alert_bar = 'This site is currently running in development mode.';
        $this->settings->search_domain = '';
        $this->settings->show_search = FALSE;

        // set local configuration
        $this->options->style_server = ANU_SM_DEFAULT_STYLE_SERVER;
        $this->options->query_param = '';
        $this->options->style_version = ANU_SM_STYLE_VERSION;
        $this->options->site_url = '#';
        $this->options->secure = FALSE;
        $this->options->return_ssl = FALSE;
        $this->options->enabled = TRUE;
        $this->options->conditions = '';
        $this->options->caching = FALSE;

        // set basic metadata
        $this->meta->subject = '';
        $this->meta->description = '';
        $this->meta->creator = 'Australian National University';
        $this->meta->extra = array();
        $this->meta->extras['robots'] = '<meta name="robots" content="noindex" />';
     }
     
   }

  
  /**
   * Set the profiles values from an array passed by a form, ready for database
   * writing
   */
  private function form_to_object($form) {
    
    // set the objects machine name
    $this->id = $form['sid'];
    $this->profile_name = $form['profile_name'];
    $this->description = $form['description'];
    
    // set template properties
    $this->settings->anu_id = $form['anu_id'];
    $this->settings->browser_title = $form['browser_title'];
    $this->settings->resp_officer = $form['resp_officer'];
    $this->settings->resp_officer_address = $form['resp_officer_address'];
    $this->settings->site_contact = $form['site_contact'];
    $this->settings->site_contact_address = $form['site_contact_address'];
    $this->settings->about_page = $form['about_page'];
    $this->settings->alert_bar = $form['alert_bar'];
    $this->settings->search_domain = $form['search_domain'];
    $this->settings->show_search = $form['show_search'];
    $this->settings->meta_subject = $form['meta_subject'];
    $this->settings->meta_description = $form['meta_description'];
    
    // set local configuration
    $this->options->style_server = $form['style_server'];
    $this->options->query_param = $form['query_param'];
    $this->options->style_version = $form['style_version'];
    $this->options->site_url = $form['site_url'];
    $this->options->secure = $form['secure'];
    $this->options->return_ssl = $form['return_ssl'];
    $this->options->enabled = $form['enabled'];
    if(isset($form['conditions'])){
      $this->options->conditions = $form['conditions'];
    }
    else {
      $this->options->conditions = NULL;
    }
    $this->options->caching = $form['caching'];
    $this->options->dev_mode = $form['dev_mode']; 
  }
  
  /**
   * Private function to clean text strings
   */
  private function clean($string) {
    return htmlentities($string, ENT_QUOTES);
  }

}