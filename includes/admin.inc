<?php 
/** 
 * Override theme_admin_menu_tree_output()
 * for compatibility with Admin module
*/
function anu_site_manager_admin_menu_tree_output($tree) {
  $output = '';
  $items = array();

  // Pull out just the menu items we are going to render so that we
  // get an accurate count for the first/last classes.
  foreach ($tree as $data) {
    if (!$data['link']['hidden']) {
      $items[] = $data;
    }
  }

  $num_items = count($items);
  foreach ($items as $i => $data) {
    $extra_class = NULL;
    if ($i == 0) {
      $extra_class = 'first';
    }
    if ($i == $num_items - 1) {
      $extra_class = 'last';
    }
    $link = theme('admin_menu_item_link', $data['link']);
    $in_active_trail = isset($data['link']['in_active_trail']) ? $data['link']['in_active_trail'] : FALSE;
    if ($data['below']) {
      $output .= theme('menu_item', $link, $data['link']['has_children'], theme('admin_menu_tree_output', $data['below']), $in_active_trail, $extra_class);
    }
    else {
      $output .= theme('menu_item', $link, $data['link']['has_children'], '', $in_active_trail, $extra_class);
    }
  }

  /* Here's the hack... 
  stop it from using our new menu tree and use Drupal default */
  //return $output ? theme('menu_tree', $output) : '';
  if ($output) {
    return '<ul class="menu">'. $output .'</ul>';
  }
}